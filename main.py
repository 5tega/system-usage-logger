import os.path

import psutil
import sys
import json
import time
#import keyboard
from logger import Logger


def main():
    if len(sys.argv) < 2:
        print("Missing required parameter: logs folder path")
        return

    logs_folder_path = sys.argv[1]
    try:
        os.makedirs(logs_folder_path)
    except OSError:
        pass

    with open("config.json") as config_file:
        config = json.load(config_file)

    interval = config["logIntervalSeconds"]
    interval = max(interval, 1)

    loggers = [
        Logger(logs_folder_path + os.path.sep + "ram_usage.log", ram_usage_getter, "Recording RAM usage into ram_usage.log")
    ]

    loggers += get_cpu_loggers(logs_folder_path)
    loggers += get_disk_loggers(logs_folder_path)

    try:
        loggers += get_temperature_loggers(logs_folder_path)
    except AttributeError as e:
        print("Can't log temperatures: unsupported on this platform")

    print("\n--Press CTRL+C to stop recording and exit--")
    counter = 0
    seconds_running = 0

    print("Running time " + get_wall_time(seconds_running), end="")

    while True:
        print("\r", end="")
        print("Running time " + get_wall_time(seconds_running), end="")
        start_time = time.time()
        time.sleep(0.01)
        end_time = time.time()
        counter += end_time - start_time
        seconds_running += end_time - start_time

        if counter >= interval:
            counter = 0
            for logger in loggers:
                logger.write_line()


def get_wall_time(seconds):
    hours = int(seconds / 3600)
    seconds -= int(hours * 3600)
    minutes = int(seconds / 60)
    seconds -= int(minutes * 60)
    seconds = int(seconds)

    return "{:02d}:{:02d}:{:02d}".format(hours, minutes, seconds)


def ram_usage_getter():
    memory_usage = psutil.virtual_memory()
    return memory_usage.used


def get_cpu_loggers(logs_folder_path):
    loggers = []
    for i in range(psutil.cpu_count(logical=True)):
        loggers.append(
            Logger(
                logs_folder_path + os.path.sep + "cpu" + str(i) + "_usage.log",
                lambda: psutil.cpu_percent(percpu=True)[i],
                "Recording CPU" + str(i) + " usage into cpu" + str(i) + "_usage.log"
            )
        )
    return loggers


def get_disk_loggers(logs_folder_path):
    loggers = []

    io_counters = psutil.disk_io_counters(perdisk=True)
    for disk_name in io_counters:
        getter = lambda: psutil.disk_io_counters(perdisk=True)[disk_name].read_bytes
        loggers.append(
            Logger(
                logs_folder_path + os.path.sep + "disk_" + disk_name + "_readbytes.log",
                getter,
                "Recording disk " + disk_name + " read bytes into " + "disk_" + disk_name + "_readbytes.log"
            )
        )

        getter = lambda: psutil.disk_io_counters(perdisk=True)[disk_name].write_bytes
        loggers.append(
            Logger(
                logs_folder_path + os.path.sep + "disk_" + disk_name + "_writebytes.log",
                getter,
                "Recording disk " + disk_name + " write bytes into " + "disk_" + disk_name + "_writebytes.log"
            )
        )
    return loggers


def get_temperature_loggers(logs_folder_path):
    loggers = []

    tempdict = psutil.sensors_temperatures()
    
    for key in tempdict.keys():
        device_list = tempdict[key]
        
        for i in range(len(device_list)):
            name = key + "_" + device_list[i].label
            getter = lambda: psutil.sensors_temperatures()[key][i].current

            loggers.append(
                Logger(
                    logs_folder_path + os.path.sep + name + "_temperature.log",
                    getter,
                    "Recording temperature for device " + name + " into " + name + "_temperature.log"
                )
            )

    return loggers


if __name__ == '__main__':
    main()

